#pragma once
#include <cstddef>
#include <new>
#include <iostream>
#include <unordered_map>


typedef unsigned long UL;
typedef char tChar;

void* _MM_MALLOC(UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line);
void* _MM_FREE(void* ptr);

void RegisterAlloc(void* Ptr, UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line);
void PrintAllocation(void* Ptr, UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line);

#define MM_MALLOC(SIZE, TYPE, DESC) _MM_MALLOC(SIZE,TYPE,DESC, __FILE__, __LINE__)
#define MM_New(CLASS, TYPE, DESC) new(TYPE, DESC, __FILE__, __LINE__) CLASS
#define Delete delete



#ifdef USE_OMEGA_ALLOCATION


inline void* operator new(size_t Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line) { std::cout << "CustomNew\n"; return _MM_MALLOC(Size, AllocType, Desc, File, Line); }
inline void* operator new[](size_t Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line) { std::cout << "CustomNew[]\n"; return _MM_MALLOC(Size, AllocType, Desc, File, Line); }
inline void operator delete(void* Ptr) { std::cout << "CustomDelete\n"; _MM_FREE(Ptr); }
inline void operator delete[](void* Ptr) { std::cout << "CustomDelete[]\n"; _MM_FREE(Ptr); }

inline void* operator new(size_t Size) 
{
	const tChar* tem = "MyType";
	return MM_MALLOC(Size, 0, tem); 
}



//void* operator new(std::size_t size);
//void* operator new(std::size_t size, const std::nothrow_t&);
//void* operator new[](std::size_t size);
//void* operator new[](std::size_t size, const std::nothrow_t&);
//
//void operator delete (void* p);
//void operator delete[](void* p);

#endif // M_ALLOC

struct ALLOCATION_INFO
{
	void* Ptr;			// address
	UL Size;		// allocation size
	//UL AllocType;
	//const char* Desc;
	//const char* File;
	//UL Line;
};


typedef std::array<ALLOCATION_INFO, 10> myArr;

class MemoryManager
{
public:
	virtual ~MemoryManager();
	static MemoryManager& getInstance();
	static void AddAllocationInfo(ALLOCATION_INFO&,const tChar*);
	static void GenerateReport();
	MemoryManager(MemoryManager const&) = delete;
	void operator=(MemoryManager const&) = delete;
private:
	//static /*std::unordered_map<tChar, ALLOCATION_INFO>*/std::list<ALLOCATION_INFO> allocatedObjs;	
	//static myArr allocatedObjs;
	static MemoryManager* instance;
	MemoryManager() {}

};



struct MemControlBlock
{
	std::size_t size_ : 31;
	bool available : 1;
};



