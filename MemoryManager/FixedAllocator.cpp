#include "FixedAllocator.h"
#include <cassert>

void* FixedAllocator::Allocate()
{
	if (allocChunk_ == 0 || allocChunk_->blocksAvailable_ == 0)
	{
		Chunks::iterator it = chunks_.begin();
		for (;; ++it)
		{
			if (it == chunks_.end())
			{
				chunks_.reserve(chunks_.size() + 1);
				Chunk newChunk;
				newChunk.Init(blockSize_, numBlocks_);
				chunks_.push_back(newChunk);
				allocChunk_ = &chunks_.back();
				deallocChunk_ = &chunks_.back();
				break;
			}

			if (it->blocksAvailable_ > 0)
			{
				allocChunk_ = &*it;
				break;
			}
		}
	}

	assert(allocChunk_ != 0);
	assert(allocChunk_->blocksAvailable_ > 0);
	return allocChunk_->Allocate(blockSize_);

}

void FixedAllocator::Deallocate(void* p)
{

	std::size_t chunkLength = numBlocks_ * blockSize_;

	auto forwardIt = allocChunk_;
	auto backwardIt = allocChunk_ - 1;

	auto firstChunk = &chunks_.front();
	auto lastChunk = &chunks_.back()+1;


	//TODO fix this and make it deallocate ONLY when 2 blocks are empty
	for (; backwardIt != NULL && forwardIt != NULL ; --backwardIt, ++forwardIt)
	{
		if (backwardIt != NULL && backwardIt >= firstChunk)
		{
			if (backwardIt->ContainsBlock(p, chunkLength))
			{
				backwardIt->Deallocate(p, blockSize_);
				return;
			}
		}
		else
		{
			backwardIt = NULL;
		}

		if (forwardIt != NULL && forwardIt < lastChunk)
		{
			if (forwardIt->ContainsBlock(p, chunkLength))
			{
				forwardIt->Deallocate(p, blockSize_);
				return;
			}
		}
		else
		{
			forwardIt = NULL;
		}

		//failed ?
	}


	//first check allocChunk;
	for (auto i = 0;i < numBlocks_;++i)
	{
		if (p == allocChunk_->pData_ + (blockSize_ * i))
		{
			allocChunk_->Deallocate(p,blockSize_);
			return;
		}
	}

	//then check prev and next chunks

	auto forwardIt = std::find(chunks_.begin(), chunks_.end(), allocChunk_);
	auto backwardIt = forwardIt;
	assert(forwardIt != chunks_.end());
	for (;(forwardIt >= chunks_.end() && (backwardIt < chunks_.begin())); forwardIt++, backwardIt--)
	{
		if (forwardIt != chunks_.end())
		{
			for (auto i = 0; i < numBlocks_; ++i)
			{
				if (p == forwardIt->pData_ + (blockSize_ * i))
				{
					forwardIt->Deallocate(p, blockSize_);
					return;
				}
			}
		}
		if (backwardIt >= chunks_.begin())
		{
			for (auto i = 0; i < numBlocks_; ++i)
			{
				if (p == backwardIt->pData_ + (blockSize_ * i))
				{
					backwardIt->Deallocate(p, blockSize_);
					return;
				}
			}
		}
	}
	while (true)
	{
		if (++forwardIt != chunks_.end())
	}


	while (true)
	{
		forwardIt.
	}

}
