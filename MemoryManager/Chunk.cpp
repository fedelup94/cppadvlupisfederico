#include "Chunk.h"
#include <cassert>

void Chunk::Init(std::size_t blockSize, unsigned char blocks)
{
	pData_ = new unsigned char[blockSize * blocks];
	firstAvailableBlock_ = 0;
	blocksAvailable_ = 0;
	unsigned char i = 0;
	unsigned char* p = pData_;

	for (; i != blocks; p += blockSize)
		*p = ++i;

}


//cost = 1 comparison + 1 index access + 2 deference + 1 assignment + 1 decrement
void* Chunk::Allocate(std::size_t blockSize)
{
	if (!blocksAvailable_) return 0;

	unsigned char* pResult = pData_ + (firstAvailableBlock_ * blockSize);

	firstAvailableBlock_ = *pResult;
	--blocksAvailable_;
	return pResult;

}

void Chunk::Deallocate(void* p, std::size_t blockSize)
{
	assert(p >= pData_);
	unsigned char* toRelease = static_cast<unsigned char*>(p);

	assert((toRelease - pData_) % blockSize == 0);
	*toRelease = firstAvailableBlock_;
	firstAvailableBlock_ = static_cast<unsigned char>(toRelease - pData_) / blockSize;
	assert(firstAvailableBlock_ == (toRelease - pData_) / blockSize);
	++blocksAvailable_;
}
