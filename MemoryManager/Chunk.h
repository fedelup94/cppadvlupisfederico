#pragma once

#include <cstddef>


struct Chunk
{
	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate(std::size_t blockSize);
	void Deallocate(void* p, std::size_t blockSize);
	inline bool ContainsBlock(void* p, std::size_t maxLength)
	{
		unsigned char* ptr = static_cast<unsigned char*>(p);
		return (pData_ < p) && (ptr < pData_ + maxLength);
	}
	unsigned char* pData_;
	unsigned char firstAvailableBlock_, blocksAvailable_;
};