#include "MemoryManager.h"
#include <iostream>

#ifdef USE_OMEGA_ALLOCATION

#endif // M_ALLOC



void* _MM_MALLOC(UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line)
{
	void* res = malloc(Size);
	PrintAllocation(res,Size,AllocType,Desc,File,Line);
	//RegisterAlloc(res,Size,AllocType,Desc,File,Line);
	return res;
}

void PrintAllocation(void* Ptr, UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line)
{
	std::size_t address = reinterpret_cast<std::size_t>(Ptr);
	std::cout << "address: " << address;
	std::cout << " size:  " << Size;
	std::cout << " allocType: " << AllocType;
	std::cout << std::endl;
}


void* _MM_FREE(void* ptr)
{
	free(ptr);
	return nullptr;
}

void RegisterAlloc(void* Ptr, UL Size, UL AllocType, const tChar* Desc, const tChar* File, UL Line)
{
	ALLOCATION_INFO info = ALLOCATION_INFO();
	info.Ptr = Ptr;
	info.Size = Size;
	MemoryManager::AddAllocationInfo(info, File);
}



MemoryManager::~MemoryManager()
{
	//if (!allocatedObjs.empty())
	//{
	//	auto it = allocatedObjs.begin();
	//	for (; it != allocatedObjs.end(); ++it)
	//	{
	//		//std::size_t address = reinterpret_cast<std::size_t>(it->second.Ptr);
	//		//std::cout << "Leaked Object: " << address << " size: " << it->second.Size << std::endl;
	//		std::size_t address = reinterpret_cast<std::size_t>(it->Ptr);
	//		std::cout << "Leaked Object: " << address << " size: " << it->Size << std::endl;
	//	}
	//}
}

MemoryManager& MemoryManager::getInstance()
{
	static MemoryManager instance;

	return instance;
}

void MemoryManager::AddAllocationInfo(ALLOCATION_INFO& info, const tChar* File)
{
	//allocatedObjs.insert(std::make_pair(*File, info));
	//std::pair<tChar, ALLOCATION_INFO> p(*File,info);
	////allocatedObjs.insert()
	//allocatedObjs.push_back(info);
	//allocatedObjs[*File] = info;
}

void MemoryManager::GenerateReport()
{
	//if (!allocatedObjs.empty())
	//{
	//	auto it = allocatedObjs.begin();
	//	for (; it != allocatedObjs.end(); ++it)
	//	{
	//		//std::size_t address = reinterpret_cast<std::size_t>(it->second.Ptr);
	//		//std::cout << "Leaked Object: " << address << " size: " << it->second.Size << std::endl;
	//		std::size_t address = reinterpret_cast<std::size_t>(it->Ptr);
	//		std::cout << "Leaked Object: " << address << " size: " << it->Size << std::endl;
	//	}
	//}
}


