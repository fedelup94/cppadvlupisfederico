#pragma once
#include <cstddef>
#include <vector>
#include "Chunk.h"

class FixedAllocator
{
public:
	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate();
	void Deallocate(void* p);
private:
	typedef std::vector<Chunk> Chunks;

	std::size_t blockSize_;
	unsigned char numBlocks_;
	Chunks chunks_;
	Chunk* allocChunk_;
	Chunk* deallocChunk_;
};

