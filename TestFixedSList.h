#pragma once
#include "SList/FixedSList.h"

class TestFixedSList;

constexpr std::size_t m_pow(std::size_t num, unsigned int pow)
{
	return (pow == 0) ? 1 : num * m_pow(num, pow - 1);
}

class TestFixedSList
{

public:

	static void Test();
	static void BenchmarkTest();
	template<typename T>
	static void InitAndAssignTest();

};

