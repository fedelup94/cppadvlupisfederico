#include "BigInteger.h"
#include "BigInteger.h"
#include <cassert>
#include <sstream>

BigInteger::BigInteger(const unsigned long long val)
{
	_actualBase = standard_bigInteger_base;
	_actualBaseDigits = standard_bigInteger_base_digits;
	isUnsigned = true;
	sign = 1;
	auto tempVal = val;

	digits = std::vector<int>();

	for (; tempVal > 0; tempVal /= _actualBase)
		digits.push_back(tempVal % _actualBase);
}

BigInteger::BigInteger(const long long val)
{
	_actualBase = standard_bigInteger_base;
	_actualBaseDigits = standard_bigInteger_base_digits;
	isUnsigned = false;
	sign = (val < 0) ? -1 : 1;


	auto tempVal = val;
	tempVal *= sign;
	digits = std::vector<int>();

	for (; tempVal > 0; tempVal /= _actualBase)
		digits.push_back(tempVal % _actualBase);
}

BigInteger::BigInteger(const std::string& str)
{
	isUnsigned = (str[0] == '-' || str[0] == '+') ? false : true;
	bool firstCharIsSign = (str[0] == '-' || str[0] == '+') ? true : false;

	if (str[0] == '-')
		sign = -1;
	else
		sign = 1;

	digits = std::vector<int>();
	_actualBase = standard_bigInteger_base;
	_actualBaseDigits = standard_bigInteger_base_digits;
	std::size_t it = (firstCharIsSign) ? 1 : 0;

	for (int i = str.size() - 1; i >= (int)it; i -= _actualBaseDigits)
	{
		long long x = 0;
		for (int j = std::max((int)it, i - _actualBaseDigits + 1); j <= i; ++j)
			x = x * 10 + str[j] - '0';

		digits.push_back(x);

	}
	adaptSize();

}

BigInteger::BigInteger(const BigInteger& other)
{
	sign = other.sign;
	isUnsigned = other.isUnsigned;
	digits = other.digits;
	_actualBase = other._actualBase;
	_actualBaseDigits = other._actualBaseDigits;
}

BigInteger::BigInteger(BigInteger&& other)
{
	sign = other.sign;
	isUnsigned = other.isUnsigned;
	_actualBase = other._actualBase;
	_actualBaseDigits = other._actualBaseDigits;
	digits = std::move(other.digits);
}

BigInteger::~BigInteger()
{
	digits.~vector();
}

bool BigInteger::operator==(const BigInteger& other) const
{
	if (sign == other.sign && isUnsigned == other.isUnsigned && digits == other.digits)
		return true;
	return false;
}

BigInteger& BigInteger::operator=(const BigInteger& other)
{
	if (*this == other)
		return *this;

	sign = other.sign;
	isUnsigned = other.isUnsigned;
	digits = other.digits;
	_actualBase = other._actualBase;
	_actualBaseDigits = other._actualBaseDigits;

	return *this;
}



BigInteger& BigInteger::operator+=(const BigInteger& other)
{
	if (sign == other.sign)
	{
		for (int i = 0, carry = 0; i < std::max(digits.size(), other.digits.size()) || carry; ++i)
		{
			//if my vector ended increase his size
			if (i == digits.size())
				digits.push_back(0);
			digits[i] += carry + (i < other.digits.size() ? other.digits[i] : 0);
			carry = digits[i] >= _actualBase;
			if (carry)
				digits[i] -= _actualBase;
		}
		return *this;
	}
	*this = *this - (-other);
	return *this;
}

BigInteger& BigInteger::operator-=(const BigInteger& other)
{
	

	if (sign == other.sign)
	{

		if (*this == abs(other))
		{
			*this = BigInteger(0);
			return *this;
		}
		if (abs() < other.abs())
		{
			*this = -other - -*this;
			return *this;
		}

		for (int i = 0, carry = 0; i < other.digits.size() || carry != 0; ++i)
		{
			digits[i] -= carry + (i < other.digits.size() ? other.digits[i] : 0);
			carry = digits[i] < 0;
			if (carry)
				digits[i] += _actualBase;
		}
		adaptSize();
		return *this;
	}

	*this = *this + (-other);
	return *this;
}

BigInteger& BigInteger::operator*=(const BigInteger& other)
{

	if (other == 0 || *this == 0)
	{
		*this = 0;
		return *this;
	}
	if (other == 1 || other == -1)
	{
		sign *= other.sign;
		return *this;
	}
	if (*this == 1 || *this == -1)
	{
		this->digits = other.digits;
		sign *= other.sign;
		return *this;
	}

	std::vector<int> a6 = change_base(this->digits, _actualBaseDigits, 6);
	std::vector<int> b6 = change_base(other.digits, _actualBaseDigits, 6);

	std::vector<long long> a(a6.begin(), a6.end());
	std::vector<long long> b(b6.begin(), b6.end());

	while (a.size() < b.size())
		a.push_back(0);
	while (b.size() < a.size())
		b.push_back(0);
	while (a.size() & (a.size() - 1))
	{
		a.push_back(0);
		b.push_back(0);
	}

	std::vector<long long> c = karatsubaMultiply(a, b);
	digits.clear();
	sign = sign * other.sign;
	for (int i = 0, carry = 0; i < (int)c.size(); ++i)
	{
		long long cur = c[i] + carry;
		digits.push_back((int)(cur % 1000000));
		carry = (int)(cur / 1000000);
	}

	digits = change_base(digits, 6, standard_bigInteger_base_digits);
	adaptSize();

	return *this;

}

BigInteger& BigInteger::operator/=(const BigInteger& other)
{
	//as mul i will call for algorithms in here
	if (other == 2)
		*this = div2(*this);
	else
		*this = repeatedDivision(*this, other).first;
	//*this = easyDivision(*this, other).first;
	return *this;
}

BigInteger& BigInteger::operator%=(const BigInteger& other)
{
	if (other == 1)
		*this = 0;
	else if (other == 2)
		*this = mod2(*this);
	else
		*this = repeatedDivision(*this, other).second;
		//*this = easyDivision(*this, other).second;
	return *this;
}

BigInteger& BigInteger::operator<<=(const BigInteger& other)
{
#ifdef USE_FIXEDSLIST
	FixedSList<bool, MAXLIST>* listz = &toBase2(this->abs());
	FixedSList<bool, MAXLIST>& list = *listz;
#else
	SList<bool> list = toBase2(this->abs());
#endif

	BigInteger exp = other;

	list.reverse();
	for (;exp > 0;exp-=1)
		list.push_front(0);

	exp = 0;
	digits.clear();

	for (auto val = list.front(); !list.empty(); list.pop_front(), ++exp)
	{
		val = list.front();
		*this += (val == false) ? 0 : pow(2, exp);
	}

#ifdef USE_FIXEDSLIST
	delete listz;
#endif // USE_FIXEDSLIST

	return *this;
}

BigInteger& BigInteger::operator>>=(const BigInteger& other)
{
	for (auto i = other.abs(); i > 0; --i)
		*this /= 2;

	return *this;
}

bool BigInteger::operator>(const BigInteger& other) const
{
	if (sign != other.sign)
		return sign > other.sign;

	if (digits.size() != other.digits.size())
		return digits.size() * sign > other.digits.size() * other.sign;	

	for (int i = digits.size() - 1; i >= 0; --i)
	{
		if (digits[i] != other.digits[i])
			return digits[i] * sign > other.digits[i] * other.sign;
	}

	return false;
}

BigInteger BigInteger::abs() const
{
	BigInteger temp = *this;
	temp.sign *= temp.sign;
	return temp;
}

BigInteger BigInteger::abs(const BigInteger& other)
{
	BigInteger temp = other;
	temp.sign *= temp.sign;
	return temp;
}

BigInteger BigInteger::pow(const BigInteger& base, const BigInteger& exponent)
{
	BigInteger result(1);

	BigInteger expAsAbs = exponent.abs();

	for (; expAsAbs > 0; --expAsAbs)
		result *= base;

	return result;
}

std::vector<int> BigInteger::change_base(const std::vector<int>& _dig, int old_base, int new_base)
{
	std::vector<long long> p(std::max(old_base, new_base) + 1);
	p[0] = 1;
	for (int i = 1; i < (int)p.size(); i++)
		p[i] = p[i - 1] * 10;
	std::vector<int> res;
	long long cur = 0;
	int cur_digits = 0;
	for (int i = 0; i < (int)_dig.size(); i++)
	{
		cur += _dig[i] * p[cur_digits];
		cur_digits += old_base;
		while (cur_digits >= new_base)
		{
			res.push_back(int(cur % p[new_base]));
			cur /= p[new_base];
			cur_digits -= new_base;
		}
	}
	res.push_back((int)cur);
	while (!res.empty() && !res.back())
		res.pop_back();
	return res;
}

#ifdef USE_FIXEDSLIST
FixedSList<bool, MAXLIST>& BigInteger::toBase2(BigInteger obj, const bool _unsigned)
{
	FixedSList<bool, MAXLIST>* list = new FixedSList<bool, MAXLIST>();
	bool wasNegative = obj.sign < 0 ? true : false;
	obj = obj.abs();

	for (; obj > 0; obj /= 2)
	{
		auto t = obj % 2;
		list->push_front((((obj % 2).digits.size() == 0) ? 0 : 1));

		//for 2 complement
		if (!_unsigned && wasNegative)
			list->front() = !list->front();
	}

	//for 2 complement
	if (!_unsigned && wasNegative)
		list->front() = !list->front();

	return *list;
}
void BigInteger::getCleanBase2List(FixedSList<bool, MAXLIST>& s1, FixedSList<bool, MAXLIST>& s2, const BigInteger& num1, const BigInteger& num2)
{
	s1 = toBase2(num1.abs(),false);
	s2 = toBase2(num2.abs(),false);

	while (s1.size() > s2.size())
		s2.push_front(0);

	while (s2.size() > s1.size())
		s1.push_front(0);

	s1.reverse();
	s2.reverse();
}
#else
SList<bool> BigInteger::toBase2(BigInteger obj, const bool _unsigned)
{
	SList<bool> list;
	bool wasNegative = obj.sign < 0 ? true : false;
	obj = obj.abs();

	for (; obj > 0;obj /= 2)
	{
		auto t = obj % 2;
		list.push_front( (((obj%2).digits.size() == 0) ? 0 : 1));
		
		//for 2 complement
		if (!_unsigned && wasNegative)
			list.front() = !list.front();
	}

	//for 2 complement
	if (!_unsigned && wasNegative)
		list.front() = !list.front();


	return list;
}

void BigInteger::getCleanBase2List(SList<bool>& s1, SList<bool>& s2, const BigInteger& num1, const BigInteger& num2)
{
	s1 = toBase2(num1, false);
	s2 = toBase2(num2, false);

	while (s1.size() > s2.size())
		s2.push_front(0);

	while (s2.size() > s1.size())
		s1.push_front(0);

	s1.reverse();
	s2.reverse();
}

#endif // USE_FIXEDSLIST


void BigInteger::resolveBitOperation(const BigInteger& num2, BIT_OPERATION operation)
{
#ifdef USE_FIXEDSLIST
	FixedSList<bool, MAXLIST>* this_listz = new FixedSList<bool, MAXLIST>();
	FixedSList<bool, MAXLIST>& this_list = *this_listz;
	FixedSList<bool, MAXLIST>* other_listz = new FixedSList<bool, MAXLIST>();
	FixedSList<bool, MAXLIST>& other_list = *other_listz;
#else
	SList<bool> this_list;
	SList<bool> other_list;
#endif

	getCleanBase2List(this_list, other_list, *this, num2);

	bool tVal;
	bool oVal;

	BigInteger exp = 0;
	digits.clear();

	for (; !this_list.empty(); this_list.pop_front(), other_list.pop_front(), ++exp)
	{
		tVal = this_list.front();
		//tVal = (sign > 0) ? tVal : !tVal;
		oVal = other_list.front();
		//oVal = (num2.sign > 0) ? oVal : !oVal;

		*this += resolveOp(tVal, oVal, operation) ? pow(2, exp) : 0;
	}

#ifdef USE_FIXEDSLIST
	delete this_listz;
	delete other_listz;
#endif // USE_FIXEDSLIST

	return;
	}
bool BigInteger::resolveOp(const bool b1, const bool b2, const BIT_OPERATION operation)
{
	switch (operation)
	{
	case BIT_OPERATION::AND:
		return (b1 && b2 && 1);
	case BIT_OPERATION::OR:
		return (b1 || b2);
	case BIT_OPERATION::XOR:
		return (!b1 != !b2);
	case BIT_OPERATION::NOT:
		return (!b1);
	default:
		break;
	}
}




// O(NlogN almost)
std::vector<long long> BigInteger::karatsubaMultiply(const std::vector<long long>& a, const std::vector<long long>& b)
{
	int n = a.size();
	std::vector<long long> res(n + n);

	// if number not so big just naive multiply --> O(digits^2)
	if (n <= 32)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				res[i + j] += a[i] * b[j];
		return res;
	}

	int k = n >> 1;
	std::vector<long long> a1(a.begin(), a.begin() + k);
	std::vector<long long> a2(a.begin() + k, a.end());
	std::vector<long long> b1(b.begin(), b.begin() + k);
	std::vector<long long> b2(b.begin() + k, b.end());

	std::vector<long long> a1b1 = karatsubaMultiply(a1, b1);
	std::vector<long long> a2b2 = karatsubaMultiply(a2, b2);

	for (int i = 0; i < k; i++)
		a2[i] += a1[i];
	for (int i = 0; i < k; i++)
		b2[i] += b1[i];

	std::vector<long long> r = karatsubaMultiply(a2, b2);
	for (int i = 0; i < (int)a1b1.size(); i++)
		r[i] -= a1b1[i];
	for (int i = 0; i < (int)a2b2.size(); i++)
		r[i] -= a2b2[i];

	for (int i = 0; i < (int)r.size(); i++)
		res[i + k] += r[i];
	for (int i = 0; i < (int)a1b1.size(); i++)
		res[i] += a1b1[i];
	for (int i = 0; i < (int)a2b2.size(); i++)
		res[i + n] += a2b2[i];
	return res;
}

std::pair<BigInteger, BigInteger> BigInteger::repeatedDivision(const BigInteger& a, const BigInteger& b)
{
	// 0 / 0 --> error
	// n / 0 --> error
	// 0 / n --> 0
	// n / n --> res

	assert((a == 0 && b != 0) || (a != 0 && b != 0), "DivError");

	if (a.abs() < b.abs())
		return std::pair<BigInteger, BigInteger>(0, a.abs());

	if (a == 0 && b != 0)
		return std::pair<BigInteger, BigInteger>(0, 0);

	BigInteger res;
	BigInteger mod = abs(a);

	BigInteger div = abs(b);

	for (; mod >= div; ++res)
		mod -= div;

	res.adaptSize();
	mod.adaptSize();
	res.sign = a.sign * b.sign;

	return std::pair<BigInteger,BigInteger>(res,mod);
}

//https://en.wikipedia.org/wiki/Long_division#Algorithm_for_arbitrary_base
std::pair<BigInteger, BigInteger> BigInteger::longDivision(const BigInteger& n, const BigInteger& m)
{
	//typedef long long ll_;
	//ll_ k = n.ToString().size();
	//ll_ l = m.ToString().size();
	//ll_ base = n._actualBase;

	//for (long long i = 0; i <= k - l; ++i)
	//{
	//	BigInteger q_;
	//	BigInteger r_;
	//	if (i == 0)
	//	{
	//		q_ = 0;
	//		r_ = 0;
	//		for (long long j = 0; j <= l - 2; ++j)
	//		{
	//			r_ += BigInteger(n.digits[i]) * pow(base, (k-j-1));
	//		}
	//		r_ = n.digits[i] * pow(base,())
	//	}
	//}

	return std::pair<BigInteger, BigInteger>();
}

std::pair<BigInteger, BigInteger> BigInteger::easyDivision(const BigInteger& n, const BigInteger& m)
{
	//auto nBase10 = change_base(n.digits, n._actualBaseDigits, 1);
	//auto mBase10 = change_base(m.digits, m._actualBaseDigits, 1);

	//BigInteger divRes;

	//for (auto i = 0; i < nBase10.size(); ++i)
	//{
	//	BigInteger exp = pow(10, i);
	//	divRes += (BigInteger(nBase10[i]) * exp);
	//}

	return std::pair<BigInteger, BigInteger>();

}

int BigInteger::mod2(const BigInteger& obj)
{
	if (obj == 0 || obj.digits.size() == 0)
		return 0;

	return obj.digits[0] % 2;

}

BigInteger BigInteger::div2(const BigInteger& obj)
{
	BigInteger temp = obj;
	for (long long i = obj.digits.size(), carry = 0; i > 0; --i)
	{
		temp.digits[i-1] += (carry != 0) ? obj._actualBase : 0;
		temp.digits[i-1] /= 2;
		carry = obj.digits[i-1] % 2;		
	}
	temp.adaptSize();
	return temp;
}

void BigInteger::adaptSize()
{
	while (!digits.empty() && !digits.back())
		digits.pop_back();
	if (digits.empty())
		sign = 1;
}

std::ostream& operator<<(std::ostream& os, const BigInteger& obj)
{
	os << obj.ToString();
	return os;
}

const std::string BigInteger::ToString() const
{
	std::string str;
	std::stringstream sstream(str);

	if (!isUnsigned)
		sstream << (sign < 0 ? "-" : "+");

	if (digits.empty())
	{
		sstream << "0";
		return sstream.str();
	}

	for (int i = digits.size(); i > 0; --i)
	{
		if (i == digits.size())
			sstream <<  digits[i - 1];
		else
			sstream << std::setw(_actualBaseDigits) << std::setfill('0') << digits[i - 1];
	}

	return sstream.str();
}
