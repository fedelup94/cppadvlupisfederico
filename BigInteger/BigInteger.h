#pragma once
#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <math.h>
#include <iomanip>
#include <algorithm>
#include "../SList/SList.h"
#include "../SList/FixedSList.h"
#include <limits>


//#define USE_FIXEDSLIST

constexpr std::size_t MAXLIST = 100000;

constexpr int standard_bigInteger_base = 1000000000;
constexpr int standard_bigInteger_base_digits = 9;

constexpr int standard_base_2 = 2;
constexpr int standard_base_2_digits = 1;



class BigInteger
{
public:
	//ctors

	BigInteger() : BigInteger(0) {}
	BigInteger(const int val) : BigInteger(static_cast<long long>(val)) {}
	BigInteger(const unsigned val): BigInteger(static_cast<unsigned long long>(val)) {}

	BigInteger(const double val) : BigInteger(static_cast<long long>(lround(val))) {}
	BigInteger(const long double val) : BigInteger(static_cast<long long>(lround(val))) {}
	BigInteger(const unsigned long long);
	BigInteger(const long long);
	BigInteger(const std::string&);
	BigInteger(const BigInteger&);
	BigInteger(BigInteger&&);

	//dtor
	virtual ~BigInteger();

	//Unary Operators
	BigInteger operator-() const { BigInteger t(*this); t.sign *= -1; t.isUnsigned = false; return t; }
	BigInteger operator+() const { return *this; }

	bool operator==(const BigInteger&) const;
	bool operator!=(const BigInteger& other) const { return !(*this == other); }

	BigInteger& operator=(const BigInteger&);
	const std::string ToString() const;

	BigInteger& operator+=(const BigInteger&);
	BigInteger operator+(const BigInteger& other) const { BigInteger t(*this); return t += other; }

	BigInteger& operator-=(const BigInteger&);
	BigInteger operator-(const BigInteger& other) const { BigInteger t(*this); return t -= other; }

	BigInteger& operator*=(const BigInteger&);
	BigInteger operator*(const BigInteger& other) const { BigInteger t(*this); return t *= other; }

	BigInteger& operator/=(const BigInteger&);
	BigInteger operator/(const BigInteger& other) const { BigInteger t(*this); return t /= other; }

	BigInteger& operator%=(const BigInteger&);
	BigInteger operator%(const BigInteger& other) const { BigInteger t(*this); return t %= other; }

	BigInteger operator++() { BigInteger t = *this; *this += 1; return t; }
	BigInteger operator++(int) { *this += 1; return *this; }

	BigInteger operator--() { BigInteger t = *this; *this -= 1; return t; }
	BigInteger operator--(int) { *this -= 1; return *this; }


	//Bitwise operators

	BigInteger& operator<<=(const BigInteger&);
	BigInteger operator<<(const BigInteger& other) const { BigInteger t(*this); return t <<= other; }

	BigInteger& operator>>=(const BigInteger&);
	BigInteger operator>>(const BigInteger& other) const { BigInteger t(*this); return t >>= other; }

	BigInteger& operator&=(const BigInteger& other) { this->resolveBitOperation(other, BIT_OPERATION::AND); return *this; }
	BigInteger operator&(const BigInteger& other) const { BigInteger t(*this); return t &= other; }

	BigInteger& operator|=(const BigInteger& other) { this->resolveBitOperation(other, BIT_OPERATION::OR); return *this; }
	BigInteger operator|(const BigInteger& other) const { BigInteger t(*this); return t |= other; }

	BigInteger& operator ^=(const BigInteger& other) { this->resolveBitOperation(other, BIT_OPERATION::XOR); return *this; }
	BigInteger operator ^(const BigInteger& other) const { BigInteger t(*this); return t ^= other; }

	//should change, lot of useless work here
	BigInteger operator~() const { BigInteger t(*this); t.resolveBitOperation(t, BIT_OPERATION::NOT); return t; }

	bool operator>(const BigInteger&) const;
	bool operator>=(const BigInteger& other) const { return ((*this == other) || (*this > other)); }
	bool operator<=(const BigInteger& other) const { return ((*this == other) || (other > *this)); }
	bool operator<(const BigInteger& other) const { return (other > *this); }

	BigInteger abs() const;

	static BigInteger abs(const BigInteger&);
	static BigInteger pow(const BigInteger& base, const BigInteger& exponent);

	

	friend std::ostream& operator<<(std::ostream& os, const BigInteger& obj);


	//UTILITY FUNCTIONS
	const std::size_t numDigits() const { return digits.size(); }

private:

	enum class BIT_OPERATION { AND, OR, XOR, NOT};

	static std::vector<long long> karatsubaMultiply(const std::vector<long long>& a, const std::vector<long long>& b);

	static std::pair<BigInteger, BigInteger> repeatedDivision(const BigInteger& a, const BigInteger& b);
	static std::pair<BigInteger, BigInteger> longDivision(const BigInteger&, const BigInteger&);
	static std::pair<BigInteger, BigInteger> easyDivision(const BigInteger&, const BigInteger&);

	static int mod2(const BigInteger&);
	static BigInteger div2(const BigInteger&);

	static std::vector<int> change_base(const std::vector<int>& _dig, int old_base, int new_base);

	void resolveBitOperation(const BigInteger&, BIT_OPERATION);
	bool resolveOp(const bool b1, const bool b2, const BIT_OPERATION operation);
	 

#ifdef USE_FIXEDSLIST
	static FixedSList<bool, MAXLIST>& toBase2(BigInteger obj, const bool _unsigned = true);
	static void getCleanBase2List(FixedSList<bool, MAXLIST>&, FixedSList<bool, MAXLIST>&, const BigInteger&, const BigInteger&);
#else
	static SList<bool> toBase2(BigInteger obj, const bool _unsigned = true);
	static void getCleanBase2List(SList<bool>&, SList<bool>&, const BigInteger&, const BigInteger&);
#endif // USE_FIXEDSLIST

	void adaptSize();



	std::vector<int> digits;
	int sign;
	bool isUnsigned;

	int _actualBase;
	int _actualBaseDigits;

};



