#include "TestBigInteger.h"
#include <cassert>

BigInteger TestBigInteger::RandomBigInt(const unsigned long long multiplier)
{
    int temp = rand();
    int newRand = rand() % 100;
    if (newRand < 50)
        temp *= -1;
    BigInteger val = BigInteger(temp);
    val *= multiplier;
    val *= (rand() % 1000) + 1;
    val *= (rand() % 1000) + 1;

    return val;
}

BigInteger TestBigInteger::RandomBigIntForPow()
{
    int temp = (rand() % 2500) + 1;
    int newRand = rand() % 100;
    if (newRand < 50)
        temp *= -1;
    BigInteger val = BigInteger(temp);

    return val;
}

BigInteger TestBigInteger::RandomBigIntUsingString(const std::size_t length)
{
    assert(length > 0, "only positive length");

    const std::string CHARACTERS = "0123456789";

    std::string randomString;

    int randomSign = rand() % 100;
    if (randomSign < 50)
        randomString += "-";
    else
        randomString += "+";

    for (int i = 1; i < length; ++i)
    {
        int randomIndex = rand() % (CHARACTERS.length() - 1);
        randomString += CHARACTERS[randomIndex];
    }

    std::cout << randomString << " ";
    return BigInteger(randomString);
}

std::string TestBigInteger::GetOperationName(Operation operation)
{
    std::string str;

    switch (operation)
    {
    case Operation::Equals:
        str = "EqualsOperation";
        break;
    case Operation::Print:
        str = "PrintOperation";
        break;
    case Operation::Sum:
        str = "Sum";
        break;
    case Operation::Subtract:
        str = "Sub";
        break;
    case Operation::Multiply:
        str = "Mul";
        break;
    case Operation::Pow:
        str = "Pow";
        break;
    case Operation::Division:
        str = "Div";
        break;
    case Operation::Modulus:
        str = "Mod";
        break;
    case Operation::LShift:
        str = "LeftShift";
        break;
    case Operation::RShift:
        str = "RightShift";
        break;
    case Operation::AND:
        str = "AND";
        break;
    case Operation::NOT:
        str = "NOT";
        break;
    case Operation::OR:
        str = "OR";
        break;
    case Operation::XOR:
        str = "XOR";
        break;
    default:
        break;
    }
    return str;
}

std::string TestBigInteger::ResolveOperation(const Operation& operation, const BigInteger& num1, const BigInteger& num2)
{
    std::string str;

    switch (operation)
    {
    case Operation::Equals:
        str = (num1 == num2) ? "equals" : "not equals";
        break;
    case Operation::Print:
        //str = num1;
        break;
    case Operation::Sum:
        str = (num1 + num2).ToString();
        break;
    case Operation::Subtract:
        str = (num1 - num2).ToString();
        break;
    case Operation::Multiply:
        str = (num1 * num2).ToString();
        break;
    case Operation::Pow:
        str = BigInteger::pow(num1,num2).ToString();
        break;
    case Operation::Division:
        str = (num1 / num2).ToString();
        break;
    case Operation::Modulus:
        str = (num1 % num2).ToString();
        break;
    case Operation::LShift:
        str = (num1 << num2).ToString();
        break;
    case Operation::RShift:
        str = (num1 >> num2).ToString();
        break;
    case Operation::AND:
        str = (num1 & num2).ToString();
        break;
    case Operation::NOT:
        str = (~num1).ToString();
        break;
    case Operation::OR:
        str = (num1 | num2).ToString();
        break;
    case Operation::XOR:
        str = (num1 ^ num2).ToString();
        break;
    default:
        break;
    }


    return str;
}

void TestBigInteger::Test(const Operation operation, const unsigned long long num1, const unsigned long long num2, const size_t caseTestNumber)
{
    for (auto i = 0; i < caseTestNumber; ++i)
    {
        BigInteger v1, v2;
        //init v1 and v2
        switch (operation)
        {
        case Operation::Pow:
            v1 = RandomBigIntForPow();
            v2 = RandomBigIntForPow().abs();
            break;
        case Operation::LShift:
            v1 = ((rand() * 1234) % 44444) + 1;
            v2 = (rand() % 850) + 1;
            //used for SLIST VS FIXEDSLIST
            //v1 = BigInteger("+1989498416511");
            //v2 = 300;
            break;
        case Operation::RShift:
            v1 = RandomBigInt(num1);
            v2 = (rand() % 100);
            break;
        case Operation::XOR:
        case Operation::NOT:
        case Operation::OR:
        case Operation::AND:
            //v1 = RandomBigIntForPow();
            //v2 = RandomBigIntForPow();
            v1 = RandomBigInt(num1);
            v2 = RandomBigInt(num2);
            v1 *= (v1 < 0) ? -1 : 1;
            v2 *= (v2 < 0) ? -1 : 1;
            break;
        default:
            v1 = RandomBigInt(num1);
            v2 = RandomBigInt(num2);
            break;
        }
        constexpr auto setWVal = 5;



        std::cout << v1
                  << std::setw(setWVal) << " "
                  << GetOperationName(operation) 
                  << std::setw(setWVal) << " "
                  << ((operation == Operation::NOT) ? "" : v2.ToString())  
                  << std::setw(setWVal) << " ";
;

        std::cout << ResolveOperation(operation, v1, v2) << std::endl;
    }

    std::cout << std::endl;
}

void TestBigInteger::BenchmarkTest()
{
    srand(time(NULL));
    constexpr size_t caseTestNumber = 30;
    constexpr size_t casePowTestNumber = 1;

    //in teoria con i bitwise dovrei usare il complemento a 2 per i negativi...
    Test(Operation::AND, 1, 1, caseTestNumber);
    Test(Operation::OR, 1, 1, caseTestNumber);
    Test(Operation::XOR, 1, 1, caseTestNumber);
    Test(Operation::NOT, 1, 1, caseTestNumber);


    Test(Operation::Sum, 1651981561, 1951261651, caseTestNumber);
    Test(Operation::Multiply, 1981263165, 198161651, caseTestNumber);
    Test(Operation::Subtract, 19816511, 19181651, caseTestNumber);

    Test(Operation::Division, 12, 15, caseTestNumber);
    Test(Operation::Modulus, 16521, 11212, caseTestNumber);
    Test(Operation::LShift, 0, 0, caseTestNumber);
    Test(Operation::RShift, 1954982, 1, caseTestNumber);
    Test(Operation::Pow, 0, 0, casePowTestNumber);

}
