#pragma once
#include "BigInteger/BigInteger.h"
#include "SList/SList.h"
#include "TestFixedSList.h"
#include "TestBigInteger.h"
#include "TestSList.h"
#include <cassert>
#include <forward_list>




//for int and other small values
//constexpr std::size_t BENCHMARK_DIM = 1 * m_pow(10, 7);
//constexpr std::size_t BENCHMARK_ELEM_TO_ERASE = 1 * m_pow(10, 5);
//constexpr std::size_t BENCHMARK_ELEM_TO_PUSH_FRONT = 5 * m_pow(10, 6);

//for string and testClass_PTR
constexpr std::size_t BENCHMARK_DIM = 1 * m_pow(10, 6);
constexpr std::size_t BENCHMARK_ELEM_TO_ERASE = 1 * m_pow(10, 4);
constexpr std::size_t BENCHMARK_ELEM_TO_PUSH_FRONT = 5 * m_pow(10, 5);