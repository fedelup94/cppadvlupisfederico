#include "TestSList.h"
#include "TestFixedSList.h"
#include "cppadvlupisfederico.h"
void TestSList::Test()
{
}

void TestSList::BenchmarkTest()
{
	InitAndAssignTest<testClass_PTR>();
	InitAndAssignTest<testClass_NO_PTR>();
	InitAndAssignTest<int>();
	//InitAndAssignTest<std::string>();
}


template<typename T>
inline void TestSList::InitAndAssignTest()
{
	std::cout << "INIT STD::FW LIST" << std::endl;
	std::forward_list<T>* stdList = new std::forward_list<T>();

	std::cout << "INIT SLIST" << std::endl;
	SList<T>* sList = new SList<T>();

	std::cout << "ASSIGN STD::FW LIST " << BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT << " elements" << std::endl;
	stdList->assign(BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT, T());

	std::cout << "ASSIGN SLIST " << BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT << " elements" << std::endl;
	sList->assign(BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT, T());

	std::cout << "ERASE " << BENCHMARK_ELEM_TO_ERASE << " elements from STD::FW LIST at begin()" << std::endl;
	for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_ERASE; ++i)
		stdList->erase_after(stdList->begin());

	std::cout << "ERASE " << BENCHMARK_ELEM_TO_ERASE << " elements from SLIST at begin()" << std::endl;
	for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_ERASE; ++i)
		sList->erase_after(sList->begin());

	std::cout << "PUSH_FRONT " << BENCHMARK_ELEM_TO_PUSH_FRONT << " elements to STD::FW LIST" << std::endl;
	for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_PUSH_FRONT; ++i)
		stdList->push_front(T());

	std::cout << "PUSH_FRONT " << BENCHMARK_ELEM_TO_PUSH_FRONT << " elements to SLIST" << std::endl;
	for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_PUSH_FRONT; ++i)
		sList->push_front(T());


	std::cout << "REVERSE STD::FW LIST" << std::endl;
	stdList->reverse();

	std::cout << "REVERSE SLIST" << std::endl;
	sList->reverse();


	std::cout << "CLEAR STD::FW LIST" << std::endl;
	stdList->clear();

	std::cout << "CLEAR SLIST" << std::endl;
	sList->clear();

	std::cout << "DELETE STD::FW LIST" << std::endl;
	delete stdList;

	std::cout << "DELETE SLIST" << std::endl;
	delete sList;

	std::cout << "";

}