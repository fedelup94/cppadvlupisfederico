#pragma once
#include "BigInteger/BigInteger.h"

constexpr unsigned int checkEqualsOperation = 99;
constexpr unsigned int printOperation = 100;
class TestBigInteger
{
public:

	enum class Operation
	{
		Print, Equals, Sum, Subtract, Multiply, Pow, Division, Modulus, LShift, RShift, AND, OR, XOR, NOT
	};

private:
	static BigInteger RandomBigInt(const unsigned long long multiplier = 0);
	static BigInteger RandomBigIntForPow();
	static BigInteger RandomBigIntUsingString(const std::size_t);
	static std::string GetOperationName(Operation operation);
	static std::string ResolveOperation(const Operation& operation, const BigInteger& num1, const BigInteger& num2);
	static void Test(const Operation operation, const unsigned long long num1, const unsigned long long num2, const size_t caseTestNumber);

public:
	static void BenchmarkTest();
};