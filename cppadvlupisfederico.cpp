#include "cppadvlupisfederico.h"

int main()
{

    // SList � abbastanza simile alla ForwardList, tempi e overhead analoghi
    TestSList::BenchmarkTest();

    // FixedList ha un overhead e tempi notevolmente inferiori in tutti i metodi tranne erase_after
    TestFixedSList::BenchmarkTest();

    // Divisione ha bisogno di un algoritmo migliore
    TestBigInteger::BenchmarkTest();

    //TestMemoryManager coming soon

    return 0;
}
