#include "TestFixedSList.h"
#include "SList/SList.h"
#include <forward_list>
#include <limits.h>
#include "cppadvlupisfederico.h"

void TestFixedSList::BenchmarkTest()
{
	InitAndAssignTest<testClass_PTR>();
	InitAndAssignTest<testClass_NO_PTR>();
	InitAndAssignTest<int>();

	//abbassare i settaggi
	//InitAndAssignTest<std::string>();

}

template<typename T>
void TestFixedSList::InitAndAssignTest()
{
	{
		std::cout << "INIT STD::FW LIST" << std::endl;
		std::forward_list<T>* stdList = new std::forward_list<T>();

		std::cout << "INIT FIXEDSLIST" << std::endl;
		FixedSList<T, BENCHMARK_DIM>* fxList = new FixedSList<T, BENCHMARK_DIM>();

		std::cout << "ASSIGN STD::FW LIST " << BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT << " elements" << std::endl;
		stdList->assign(BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT, T());

		std::cout << "ASSIGN FIXEDSLIST " << BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT << " elements" << std::endl;
		fxList->assign(BENCHMARK_DIM - BENCHMARK_ELEM_TO_PUSH_FRONT, T());

		std::cout << "ERASE " << BENCHMARK_ELEM_TO_ERASE << " elements from STD::FW LIST at begin()" << std::endl;
		for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_ERASE; ++i)
			stdList->erase_after(stdList->begin());

		std::cout << "ERASE " << BENCHMARK_ELEM_TO_ERASE << " elements from FIXEDLIST at begin()" << std::endl;
		for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_ERASE; ++i)
			fxList->erase_after(fxList->begin());

		std::cout << "PUSH_FRONT " << BENCHMARK_ELEM_TO_PUSH_FRONT << " elements to STD::FW LIST" << std::endl;
		for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_PUSH_FRONT; ++i)
			stdList->push_front(T());

		std::cout << "PUSH_FRONT " << BENCHMARK_ELEM_TO_PUSH_FRONT << " elements to FIXEDLIST" << std::endl;
		for (std::size_t i = 0; i < BENCHMARK_ELEM_TO_PUSH_FRONT; ++i)
			fxList->push_front(T());


		std::cout << "REVERSE STD::FW LIST" << std::endl;
		stdList->reverse();

		std::cout << "REVERSE FIXEDLIST" << std::endl;
		fxList->reverse();


		std::cout << "CLEAR STD::FW LIST" << std::endl;
		stdList->clear();

		std::cout << "CLEAR FIXEDLIST" << std::endl;
		fxList->clear();

		std::cout << "DELETE STD::FW LIST" << std::endl;
		delete stdList;

		std::cout << "DELETE FIXEDLIST" << std::endl;
		delete fxList;

		fxList = new FixedSList<T, BENCHMARK_DIM>();
		std::cout << "EXTRA, UPSIZE FIXEDLIST FROM " << BENCHMARK_DIM << " TO " << BENCHMARK_DIM * 2 << std::endl;

		FixedSList<T, BENCHMARK_DIM * 2> *newFxList;
		newFxList = &FixedSList<T, BENCHMARK_DIM * 2>::upsize<BENCHMARK_DIM>(*fxList);

		delete fxList;
		std::cout << "";

		delete newFxList;
	}
	
	std::cout << "";


}














void TestFixedSList::Test()
{





	//std::string* p1 = new std::string("1");
	//std::string* p2 = new std::string("2");
	//std::string* p3 = new std::string("3");
	//std::string* p4 = new std::string("4");
	//std::string* p5 = new std::string("5");

	//FixedSList<int, 10> f1(6,99);
	FixedSList<int, 10> f1;
	f1.push_front(5);
	f1.push_front(4);
	f1.push_front(3);
	f1.push_front(2);
	f1.push_front(1);

	auto its = std::next(f1.begin(), 1);

	//f1.shiftLeft(its, 1);
	std::cout << f1 << std::endl;
	f1.erase_after(its);
	f1.erase_after(f1.begin());
	std::cout << f1 << std::endl;

	f1.insert_after(f1.begin(), 88);
	f1.insert_after(f1.begin(), 89);
	f1.insert_after(f1.begin(), 90);


	std::cout << f1 << std::endl;

	int* pippo = new int(10);

	f1.push_front(*pippo);

	std::cout << f1 << std::endl;


	FixedSList<int, 10>* f2 = new FixedSList<int, 10>(8, 97);

	auto it = std::next(f1.begin(), 2);
	std::cout << f1 << std::endl;
	f1.insert_after(it, f2->begin()->value);

	std::cout << f1 << std::endl;

	f1.push_front(33);
	std::cout << f1 << std::endl;

	//FixedSList<int,18> giggi = FixedSList<int, 10>::upsize<18>();

	//FixedSList<int, 18> giggi = f1.upsize<18>();

	FixedSList<int, 18> ps;

	//FixedSList<int, 10>::upsize(ps, f1);

	//f1.upsize<18>();

	ps = ps.upsize(f1);

	delete pippo;

	//f1.upsize();
//
//
//	//giggi.push_front(13);
//
//	//FixedSList<int, 10> f2 = std::move(f1);
//
//	//FixedSList<int, 10> f2{ 1,2,3,4,5,6 };
//
//	//FixedSList<std::string,10> words{ "the", "frogurt", "is", "also", "cursed" };
//
//
//	//FixedSList<std::string, 10> f2 = words;
//
//
//	//std::cout << f2 << std::endl;
//
//	//f2.insert_after(f2.begin(), "KEBAB");
//
//	//std::cout << f2 << std::endl;
//
//	//f1.push_front(37);
//
//	//f1.clear();
//
//	//FixedSList<int,10> f3 = std::move(f2);
//
//	//FixedSList<int, 5> f4 = { 1,2,3,4,5,6,7,8 };
//
//
//	//FixedSList<std::string*, 10> f5;
//	//{
//	//	std::string a = "PROVA";
//	//	std::string* k = new std::string("PROVA");
//
//	//	f5.push_front(k);
//	//}
//
//	//auto it = f1.before_begin();
//
//	//for (;; ++it)
//	//{
//	//	std::cout << "it: " << it->value << std::endl;
//	//}
//
//	//++it;
//	//++it;
//
//
//
//
//
//	//FixedSList<int, 10> p1{ };
//	//FixedSList<int, 10> p2{ 4, 99 };
//
//
//	//FixedSList<int, 5> p3(4, 3);
//
//	//FixedSList<int, 5> p4 = p3;
//
//	////std::cout << p.front();
//
//	//std::cout << p2 << std::endl;
//
//	////p2.assign(11, 1);
//
//	//std::cout << p2 << std::endl;
//
//	//auto it = p2.before_begin();
//	////++it;
//	//std::cout << it->value;
//	//FixedSList<int,10> p3 = std::move(FixedSList<int,10>(6,123));
//
//	//std::cout << p3;

}
