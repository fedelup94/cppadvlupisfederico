#pragma once
#include "baseSList.h"


template <typename T>
class SList : public baseSList<T>
{
private:
public:

	typedef SListIterator<T> iterator;


	//Ctors

	SList() : head(new Node<T>()), size_(0) {}
	SList(std::size_t count, const T& value) : head(new Node<T>()), size_(0) { for (; count > 0; --count) push_front(value); }
	SList(std::size_t count) : SList(count,T()) {}
	SList(const SList& other);
	SList(SList&& other) { head = other.head; size_ = other.size_; other.head = NULL; }
	SList(std::initializer_list<T> init);


	//Member Functions

	SList& operator=(const SList& other);
	SList& operator=(SList&& other);
	virtual void assign(std::size_t count, const T& value) override;


	//Element access

	virtual T& front() override { return begin()->value; }
	virtual const T& front() const override { return begin()->value; }


	//Iterators

	virtual iterator before_begin() override { return iterator(head); }
	virtual const iterator before_begin() const override  { return  iterator(head); }
	virtual iterator begin() override { return iterator(head->next); }
	virtual const iterator begin() const override { return  iterator(head->next); }
	virtual iterator end() override { return iterator(nullptr); }
	virtual const iterator end() const override { return iterator(nullptr); }


	//Capacity

	virtual bool empty() const override { return size_ == 0; }
	virtual std::size_t max_size() const override { return std::numeric_limits<std::ptrdiff_t>::max(); }


	//Modifiers

	virtual void clear() override;
	virtual iterator insert_after(const iterator pos, const T& value) override;
	virtual iterator insert_after(const iterator pos, T&& value) override;
	virtual iterator insert_after(const iterator pos, std::size_t count, const T& value) override;

	virtual iterator erase_after(const iterator pos) override;
	virtual iterator erase_after(const iterator first, const iterator last) override;

	virtual void push_front(const T& value) override;
	virtual void push_front(T&& value) override;

	virtual void pop_front() override { erase_after(before_begin()); }

	virtual void resize(std::size_t count) override { resize(count, T()); }
	virtual void resize(std::size_t count, const T& value) override;
	void swap(SList& other) { std::swap(head, other.head); std::swap(size_, other.size_); }

	//Operations

	virtual void reverse() override;
	virtual void unique() override { return unique(std::equal_to<T>()); }
	template<class BinaryPredicate>
	void unique(BinaryPredicate p);

	//input iterators?


	virtual ~SList();

	template<typename U>
	friend std::ostream& operator<<(std::ostream&, const SList<U>&);



	//NOT FROM STANDARD
	const std::size_t size() const { return size_; }

private:
	Node<T>* head;
	std::size_t size_;

};


#pragma region Constructors

template<typename T>
inline SList<T>::SList(const SList& other)
{
	auto temp = new SList<T>(other.size_);
	auto nl_it = temp->begin();

	for (auto o_it = other.begin(); o_it != nullptr; ++o_it, ++nl_it)
		nl_it->value = o_it->value;

	this->size_ = temp->size_;
	this->head = temp->head;
	temp->head = NULL;
}

template<typename T>
inline SList<T>::SList(std::initializer_list<T> init)
{
	head = new Node<T>();
	size_ = 0;

	for (auto it = init.end() - 1; it != init.begin(); --it)
		push_front(*it);
	push_front(*init.begin());
}

#pragma endregion

#pragma region Member Functions

template<typename T>
inline SList<T>& SList<T>::operator=(const SList<T>& other)
{
	SList* list = new SList(other);
	*this = std::move(*list);
	return *this;
}

template<typename T>
inline SList<T>& SList<T>::operator=(SList<T>&& other)
{
	head = other.head;
	other.head = NULL;

	size_ = other.size_;
	other.~SList();
	return *this;
}

template<typename T>
inline void SList<T>::assign(std::size_t count, const T& value)
{
	clear();
	for (; count > 0; --count)
		push_front(value);
}

#pragma endregion

#pragma region Modifiers

template<typename T>
inline void SList<T>::clear()
{
	size_ = 0;

	if (!head->next)
		return;

	Node<T>* currentNode = head->next;
	while (currentNode != NULL)
	{
		Node<T>* tempNode = currentNode;
		currentNode = currentNode->next;
		tempNode->value.~T();
		delete tempNode;
	}

	head->next = NULL;

}

template<typename T>
inline typename SList<T>::iterator SList<T>::insert_after(const iterator pos, const T& value)
{
	++size_;
	iterator it(pos);
	Node<T>* t = new Node<T>(value, it->next);
	it->next = t;
	return ++it;
}

template<typename T>
inline typename SList<T>::iterator SList<T>::insert_after(const iterator pos, T&& value)
{
	++size_;
	iterator it(pos);
	Node<T>* t = new Node<T>(std::move(value), it->next);
	it->next = t;
	it++;
	return it;
}

template<typename T>
inline typename SList<T>::iterator SList<T>::insert_after(const iterator pos, std::size_t count, const T& value)
{
	iterator it = pos;
	for (; count > 0; count--)
		it = insert_after(it, value);

	return it;
}

template<typename T>
inline typename SList<T>::iterator SList<T>::erase_after(const iterator pos)
{
	iterator it = pos;

	if (it->next == NULL)
	{
		return end();
	}

	size_--;
	iterator temp = it->next;
	Node<T>* nodeToDelete = it->next;

	if (it->next->next != NULL)
		it->next = it->next->next;
	else
		it->next = NULL;
	temp->value.~T();

	delete nodeToDelete;
	return it;
}

template<typename T>
inline typename SList<T>::iterator SList<T>::erase_after(const iterator first, const iterator last)
{
	iterator it = first;
	iterator nextIt = it;
	while (nextIt != last && it->next != NULL)
	{
		it = erase_after(it);
		nextIt = it->next;
	}

	return it;
}

template<typename T>
inline void SList<T>::push_front(const T& value)
{
	size_++;
	Node<T>* t = new Node<T>(value, head->next);
	head->next = t;
}

template<typename T>
inline void SList<T>::push_front(T&& value)
{
	size_++;
	Node<T>* t = new Node<T>(std::move(value), head->next);

	head->next = t;
}

template<typename T>
inline void SList<T>::resize(std::size_t count, const T& value)
{
	auto it = before_begin();

	if (size_ < count)
	{
		while (it->next != NULL)
			++it;
		while (size_ != count)
			insert_after(it, value);
	}
	else
	{
		it = std::next(before_begin(), count);
		erase_after(it, end());
	}
}

#pragma endregion

#pragma region Operations

template<typename T>
inline void SList<T>::reverse()
{
	Node<T>* current = head->next;
	Node<T>* prev = NULL;
	Node<T>* next = NULL;

	while (current != NULL)
	{
		next = current->next;
		current->next = prev;

		prev = current;
		current = next;
	}
	head->next = prev;
}

template<typename T>
template<class BinaryPredicate>
inline void SList<T>::unique(BinaryPredicate pred)
{

	iterator it = before_begin();
	auto nextIt = it;
	++nextIt;
	while (nextIt != NULL)
	{
		if (pred(it->value, nextIt->value))
		{
			nextIt = erase_after(it);
		}
		else
		{
			it = nextIt;
			++nextIt;
		}
	}

}

#pragma endregion

template<typename T>
inline SList<T>::~SList()
{
	if (!head)
		return;

	Node<T>* currentNode = head;

	while (currentNode != NULL)
	{
		Node<T>* tempNode = currentNode;
		currentNode = currentNode->next;
		tempNode->value.~T();
		delete tempNode;
	}
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const SList<T>& obj)
{
	os << "[ ";
	if (!obj.empty())
	{
		for (auto it = obj.begin();it != nullptr; ++it)
		{
			os << it->value << " ";
		}
	}
	os << "]";

	return os;
}


