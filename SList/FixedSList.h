#pragma once
#include "baseSList.h"

template<typename T, std::size_t N>
class FixedSList : public baseSList<T>
{

public:
	typedef SListIterator<T> iterator;

	//Ctors

	FixedSList() : m_data(), size_(0), head(new Node<T>()), emptyNodes(new Node<T>()) {}
	FixedSList(std::size_t count, const T& value);
	FixedSList(std::size_t count) : FixedSList(count,T()) {}
	FixedSList(const FixedSList& other);
	FixedSList(FixedSList&& other);
	FixedSList(std::initializer_list<T> init);


	//Member functions

	FixedSList& operator=(const FixedSList& other);
	FixedSList& operator=(FixedSList&& other);
	virtual void assign (std::size_t count, const T& value) override;


	// Element access
	
	virtual T& front() override { return begin()->value; }
	virtual const T& front() const override { return begin()->value; }


	//Iterators

	virtual iterator before_begin() override { iterator p(head); return p; }
	virtual const iterator before_begin() const override { iterator p(head); return p; }
	virtual iterator begin() override { return  iterator(head->next); }
	virtual const iterator begin() const override { return  iterator(head->next); }
	virtual iterator end() override { return  iterator(NULL); }
	virtual const iterator end() const override { return  iterator(NULL); }


	//Modifiers

	bool full() const { return size_ == N; }
	virtual bool empty() const override { return size_ == 0; }
	virtual std::size_t max_size() const override { return N; }
	virtual void clear() override;
	virtual iterator insert_after(const iterator pos, const T& value) override { return insert_after(pos, T(value)); }
	virtual iterator insert_after(const iterator pos, T&& value) override;
	virtual iterator insert_after(const iterator pos, std::size_t count, const T& value) override;
	virtual iterator erase_after(const iterator pos) override;
	virtual iterator erase_after(const iterator first, const iterator last) override;
	virtual void push_front(const T& value) override { push_front(T(value)); }
	virtual void push_front(T&& value) override;
	virtual void pop_front() override { erase_after(before_begin()); }
	virtual void resize(std::size_t count) override { resize(count, T()); }
	virtual void resize(std::size_t count, const T& value) override;


	// Operations
	//non funziona ancora
	virtual void reverse() override;
	template<class BinaryPredicate>
	void unique(BinaryPredicate p);
	virtual void unique() override { return unique(std::equal_to<T>()); }


	~FixedSList();

	template<std::size_t oldN>
	static FixedSList& upsize(const FixedSList<T, oldN>&);

	template<typename U, std::size_t N>
	friend std::ostream& operator<<(std::ostream&, const FixedSList<U,N>&);

	//NOT FROM STANDARD
	const std::size_t size() const { return size_; }


private:
	void addToEmptyNodes(const iterator pos);

	
	Node<T>* head;
	Node<T> m_data[N];
	std::size_t size_;
	Node<T>* emptyNodes;

};

#pragma region Constructors

template<typename T, std::size_t N>
inline FixedSList<T, N>::FixedSList(std::size_t count, const T& value)
{
	head = new Node<T>();
	size_ = 0;
	emptyNodes = new Node<T>();
	for (; count > 0; --count) 
		push_front(value);
}

template<typename T, std::size_t N>
inline FixedSList<T, N>::FixedSList(const FixedSList& other)
{
	size_ = 0;
	head = new Node<T>(NULL, &m_data[0]);
	emptyNodes = new Node<T>();


	for (auto it = other.begin(); it != other.end(); ++it)
	{
		m_data[size_].value = it->value;
		m_data[size_].next = (it->next == NULL) ? NULL : &m_data[size_ + 1];
		size_++;
	}

}

template<typename T, std::size_t N>
inline FixedSList<T, N>::FixedSList(FixedSList&& other)
{
	size_ = 0;
	head = new Node<T>(NULL, &m_data[0]);
	emptyNodes = new Node<T>();

	for (auto it = other.begin(); it != other.end(); ++it, ++size_)
	{
		if (std::is_pod<T>())
			m_data[size_].value = std::move(it->value);
		else
			m_data[size_].value = it->value;
		m_data[size_].next = (it->next == NULL) ? NULL : &m_data[size_ + 1];
	}
}

template<typename T, std::size_t N>
inline FixedSList<T, N>::FixedSList(std::initializer_list<T> init)
{
	auto listSize = std::min(init.size(),N);	
	auto it = init.begin();

	for (size_ = 0; it != init.end() && size_ + 1 <= listSize; ++it, size_++)
	{
		m_data[size_].value = *it;
		m_data[size_].next = (size_ + 1 == N) ? NULL : &m_data[size_ + 1];
	}

	head = new Node<T>(T(), &m_data[0]);
	emptyNodes = new Node<T>();

}

#pragma endregion


#pragma region Member Functions

template<typename T, std::size_t N>
inline FixedSList<T,N>& FixedSList<T, N>::operator=(const FixedSList& other)
{
	clear();

	for (auto it = other.begin(); it != other.end(); ++it, ++size_)
	{
		m_data[size_].value = it->value;
		m_data[size_].next = (size_ + 1 == N) ? NULL : &m_data[size_ + 1];
	}
	head->next = &m_data[0];

	return *this;
}

template<typename T, std::size_t N>
inline FixedSList<T,N>& FixedSList<T, N>::operator=(FixedSList&& other)
{
	clear();

	for (auto it = other.begin(); it != other.end(); ++it, ++size_)
	{
		m_data[size_].value = it->value;
		m_data[size_].next = (it->next == NULL) ? NULL : &m_data[size_ + 1];
	}
	head->next = &m_data[0];

	return *this;

}

template<typename T, std::size_t N>
inline void FixedSList<T, N>::assign(std::size_t count, const T& value)
{
	clear();
	auto maxDim = std::min(N, count);
	for (size_ = 0; size_ < maxDim; ++size_)
	{
		m_data[size_].value = value;
		m_data[size_].next = (size_ + 1 == maxDim) ? NULL : &m_data[size_ + 1];
	}
	head->next = &m_data[0];
	emptyNodes->next = NULL;
}

#pragma endregion


#pragma region Modifiers

template<typename T, std::size_t N>
inline void FixedSList<T, N>::clear()
{
	for (auto i = 0; i < size_; ++i)
		m_data[i].value.~T();

	head->next = NULL;
	emptyNodes->next = NULL;
	size_ = 0;
}

template<typename T, std::size_t N>
inline typename FixedSList<T, N>::iterator FixedSList<T, N>::insert_after(const iterator pos, T&& value)
{
	if (size_ == N)
		return iterator(NULL);

	iterator it(pos);


	if (emptyNodes->next != NULL)
	{
		Node<T>* tempNext = emptyNodes->next->next;
		Node<T>* temp = std::move(emptyNodes->next);

		emptyNodes = emptyNodes->next;
		emptyNodes->value = std::move(value);
		emptyNodes->next = (size_ + 1 == NULL) ? NULL : it->next;
		it->next = std::move(emptyNodes);
		Node<T>* newEmpty = new Node<T>(NULL,tempNext);
		emptyNodes = newEmpty;
		if (tempNext == NULL)
			delete tempNext;
		size_++;

	}
	else
	{
		m_data[size_].value = std::move(value);
		m_data[size_].next = (size_ + 1 == N) ? NULL : it->next;
		it->next = &m_data[size_];
		size_++;
	}


	return ++it;
}

template<typename T, std::size_t N>
inline typename FixedSList<T, N>::iterator FixedSList<T, N>::insert_after(const iterator pos, std::size_t count, const T& value)
{
	iterator it = pos;
	for (; count > 0; count--)
		it = insert_after(it, value);

	return it;
}

template<typename T, std::size_t N>
inline typename FixedSList<T,N>::iterator FixedSList<T, N>::erase_after(const iterator pos)
{

	iterator it = pos;

	if (it->next == NULL)
		return end();

	--size_;
	iterator temp = it->next;
	addToEmptyNodes(pos);

	if (it->next->next != NULL)
		it->next = it->next->next;
	else
		it->next = NULL;
	temp->value.~T();
	temp->next = NULL;

	return it;

}

template<typename T, std::size_t N>
inline typename FixedSList<T,N>::iterator FixedSList<T, N>::erase_after(const iterator first, const iterator last)
{
	iterator it = first;
	iterator nextIt = it;
	while (nextIt != last && it->next != NULL)
	{
		it = erase_after(it);
		nextIt = it->next;
	}

	return it;
}

template<typename T, std::size_t N>
inline void FixedSList<T, N>::push_front(T&& value)
{
	if (size_ == N)
	{
		std::cout << "MAXSIZE";
		return;
	}

	m_data[size_].value = value;
	m_data[size_].next = head->next;
	head->next = &m_data[size_];
	size_++;
}

template<typename T, std::size_t N>
inline void FixedSList<T, N>::resize(std::size_t count, const T& value)
{
	if (count > N)
		return;

	auto it = before_begin();

	if (size_ < count)
	{
		//it = std::next(it, size_ - 1);
		while (it->next != NULL)
			++it;
		while (size_ != count)
			insert_after(it, value);
	}
	else
	{
		it = std::next(before_begin(), count);
		//erase_after(it, end());
	}
}

template<typename T, std::size_t N>
inline void FixedSList<T, N>::reverse()
{
	// H -> 1 -> 2 -> 3 -> 4 -> 5
	//i1				i2
	//c = 1 -> 2		c=
	//n = 2				n=3
	//p = 1


	Node<T>* current = head->next;
	Node<T>* prev = NULL;
	Node<T>* next = NULL;

	while (current->next != NULL)
	{
		next = current->next;
		current->next = prev;

		prev = current;
		current = next;
	}
	head->next = prev;

}

template<typename T, std::size_t N>
template<class BinaryPredicate>
inline void FixedSList<T, N>::unique(BinaryPredicate pred)
{
	iterator it = before_begin();
	auto nextIt = it;
	++nextIt;
	while (nextIt != NULL)
	{
		if (pred(it->value, nextIt->value))
		{
			nextIt = erase_after(it);
		}
		else
		{
			it = nextIt;
			++nextIt;
		}
	}
}

#pragma endregion


template<typename T, std::size_t N>
inline FixedSList<T, N>::~FixedSList()
{
	for (auto i = 0; i < size_; ++i)
		m_data[i].value.~T();

	delete head;
	delete emptyNodes;
}

template<typename T, std::size_t N>
inline void FixedSList<T, N>::addToEmptyNodes(const iterator pos)
{
	auto node = emptyNodes;
	for (; node->next != NULL; node = node->next)
	{ }

	node->next = pos->next;
}


template<typename T, std::size_t N>
std::ostream& operator<<(std::ostream& os, const FixedSList<T, N>& obj)
{
	os << "[ ";

	for (auto it = obj.begin(); it != nullptr; ++it)
		os << it->value << " ";

	os << "]";

	os << " size: " << obj.size_;

	return os;
}

template<typename T, std::size_t N>
template<std::size_t oldN>
inline FixedSList<T,N>& FixedSList<T,N>::upsize(const FixedSList<T, oldN>& other)
{

	static_assert(N > oldN, "Invalid new Size");

	FixedSList* newList = new FixedSList();

	if (!other.empty())
	{
		for (auto it = other.begin(); it != other.end(); ++it)
		{
			newList->m_data[newList->size_].value = std::move(it->value);
			newList->m_data[newList->size_].next = (it->next == NULL) ? NULL : &newList->m_data[newList->size_ + 1];
			newList->size_++;
		}
		newList->head->next = &newList->m_data[0];
	}

	return *newList;
}
