#pragma once
#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <math.h>
#include <iomanip>
#include <algorithm>
#include <unordered_map>

template <typename T>
struct Node
{
	T value;
	Node* next;
	Node() : value(), next(NULL) {}
	Node(T val) : value(val), next(NULL) {}
	Node(T val, Node* ptr) : value(val), next(ptr) {}
	Node(const Node<T>& other) : value(other.value), next(other.next) {}
	Node(Node<T>&& other) : value(other.value), next(other.next) { other.next = NULL; }
	Node& operator=(const Node& other) { value = other.value; next = other.next; return *this; }
	Node& operator=(Node&& other) { value = other.value; next = other.next; other.next = NULL; return *this; }

	~Node() { value.~T(); }

};


class testClass_NO_PTR final
{
public:
	testClass_NO_PTR() : i(10), s("TEMPVAL") {}
	testClass_NO_PTR(const int _i) : i(_i), s("TEMPVAL") {}
	testClass_NO_PTR(const std::string _s) :i(10), s(_s) {}
	testClass_NO_PTR(const int _i, const std::string _s) : i(_i), s(_s) {}
	testClass_NO_PTR(const testClass_NO_PTR& other) : i(other.i), s(other.s) {}
	testClass_NO_PTR(testClass_NO_PTR&& other) noexcept : i(other.i), s(std::move(other.s)) {}

	testClass_NO_PTR& operator=(const testClass_NO_PTR& other) { i = other.i; s = other.s; return *this; }
	testClass_NO_PTR& operator=(testClass_NO_PTR&& other) noexcept { i = other.i; s = std::move(other.s); return *this; }

	bool operator==(const testClass_NO_PTR& other) const { return (i == other.i) && (s == other.s); }
	bool operator!=(const testClass_NO_PTR& other) const { return !(*this == other); }

	~testClass_NO_PTR() { }

private:
	int i;
	std::string s;
};

class testClass_PTR final
{
public:
	testClass_PTR() : i_ptr(new int(10)), s_ptr(new std::string("TEMPVAL")) {}
	testClass_PTR(const int i) : i_ptr(new int(i)), s_ptr(new std::string("TEMPVAL")) {}
	testClass_PTR(const std::string s) : i_ptr(new int(10)), s_ptr(new std::string(s)) {}
	testClass_PTR(const int i, const std::string s) : i_ptr(new int(i)), s_ptr(new std::string(s)) {}
	testClass_PTR(const testClass_PTR& other) : i_ptr(new int(*other.i_ptr)), s_ptr(new std::string(*other.s_ptr)) {}
	testClass_PTR(testClass_PTR&& other) noexcept : i_ptr(std::move(other.i_ptr)), s_ptr(std::move(other.s_ptr)) { other.i_ptr = nullptr; other.s_ptr = nullptr; }
	//testClass_PTR(testClass_PTR&& other) noexcept { i_ptr = std::move(other.i_ptr); s_ptr = std::move(other.s_ptr); }

	testClass_PTR& operator=(const testClass_PTR& other) { *i_ptr = *other.i_ptr; *s_ptr = *other.s_ptr; return *this; }
	testClass_PTR& operator=(testClass_PTR&& other) noexcept { i_ptr = std::move(other.i_ptr); s_ptr = std::move(other.s_ptr); return *this; }

	bool operator==(const testClass_PTR& other) const { return ((*i_ptr == *other.i_ptr) && (*s_ptr == *other.s_ptr)); }
	bool operator!=(const testClass_PTR& other) const { return !(*this == other); }


	~testClass_PTR() { delete i_ptr; delete s_ptr; i_ptr = nullptr; s_ptr = nullptr; }
private:
	int* i_ptr;
	std::string* s_ptr;
};



template<typename T>
class SList;




template<typename T>
class SListIterator : public std::iterator<std::forward_iterator_tag, Node<T>, std::ptrdiff_t, Node<T>*, Node<T>&>
{
public:
	SListIterator() : m_ptr(NULL) {}
	SListIterator(Node<T>* ptr) : m_ptr(ptr) {}
	SListIterator operator++() { SListIterator i = *this; m_ptr = m_ptr->next; return i; }
	SListIterator operator++(int) { m_ptr = m_ptr->next; return *this; }
	Node<T>& operator*() { return *m_ptr; }
	const Node<T>& operator*() const { return *m_ptr; }
	Node<T>* operator->() { return m_ptr; }
	const Node<T>* operator->() const { return m_ptr; }
	bool operator==(const SListIterator& rhs) { return m_ptr == rhs.m_ptr; }
	bool operator!=(const SListIterator& rhs) { return m_ptr != rhs.m_ptr; }
private:
	Node<T>* m_ptr;
};


template <typename T>
class baseSList
{
public:

	typedef SListIterator<T> iterator;


	//Ctors

	baseSList() {}
	baseSList(std::size_t count, const T& value) {}
	baseSList(std::size_t count) {}
	baseSList(const baseSList& other) {}
	baseSList(baseSList&& other) {}
	baseSList(std::initializer_list<T> init) {}


	//Member Functions

	//virtual baseSList& operator=(const baseSList& other) = 0;
	//virtual baseSList& operator=(baseSList&& other) = 0;
	virtual void assign(std::size_t count, const T& value) {}


	//Element access

	virtual T& front() = 0;
	virtual const T& front() const = 0;


	//Iterators

	virtual iterator before_begin() { return iterator(); }
	virtual const iterator before_begin() const { return iterator(); }
	virtual iterator begin() { return iterator(); }
	virtual const iterator begin() const { return iterator(); }
	virtual iterator end() { return iterator(); }
	virtual const iterator end() const { return iterator(); }


	//Capacity

	virtual bool empty() const { return false; }
	virtual std::size_t max_size() const { return 0; }


	//Modifiers

	virtual void clear() {}
	virtual iterator insert_after(const iterator pos, const T& value) { return iterator(); }
	virtual iterator insert_after(const iterator pos, T&& value) { return iterator(); }
	virtual iterator insert_after(const iterator pos, std::size_t count, const T& value) { return iterator(); }

	virtual iterator erase_after(const iterator pos) { return iterator(); }
	virtual iterator erase_after(const iterator first, const iterator last) { return iterator(); }

	virtual void push_front(const T& value) {}
	virtual void push_front(T&& value) {}

	virtual void pop_front() {}

	virtual void resize(std::size_t count) {}
	virtual void resize(std::size_t count, const T& value) {}
	//virtual void swap(baseSList& other) = 0;


	//Operations

	virtual void reverse() {}
	virtual void unique() {}
	//template<class BinaryPredicate>
	//virtual void unique(BinaryPredicate p) = 0;

	virtual ~baseSList() {}

	//template<typename U>
	//virtual friend std::ostream& operator<<(std::ostream&, const baseSList<U>&) = 0;

};

